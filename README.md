# sp_log_analyzer

This is a Ruby app that analyzes logs in a specific format. It was created as a solution for recruitment test by Smart Pension.

## Usage

`ruby parser.rb path/to/webserver.log`

## Development setup

`bundle install`

## Tests

`rspec`

## Lint

`rubocop`
