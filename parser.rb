# frozen_string_literal: true

require_relative 'lib/sp_log_analyzer/application'

SpLogAnalyzer::Application.new(log_path: ARGV[0]).call
