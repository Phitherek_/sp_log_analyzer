# frozen_string_literal: true

module SpLogAnalyzer
  module Services
    class BaseService
      def initialize(**args); end

      def self.call(**args)
        new(**args).call
      end
    end
  end
end
