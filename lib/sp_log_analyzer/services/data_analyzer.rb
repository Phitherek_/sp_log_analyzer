# frozen_string_literal: true

require_relative 'base_service'
require_relative 'total_views_calculator'
require_relative 'unique_views_calculator'
require_relative 'data_sorter'
require_relative 'data_printer'

module SpLogAnalyzer
  module Services
    class DataAnalyzer < BaseService
      attr_reader :log_data, :mode

      def initialize(log_data: [], mode: nil)
        @log_data = log_data
        @mode = mode
        super()
      end

      def call
        return false if calculator(mode).nil?

        views_data = calculator(mode).call(log_data: log_data)
        sorted_views_data = ::SpLogAnalyzer::Services::DataSorter.call(views_data: views_data)
        ::SpLogAnalyzer::Services::DataPrinter.call(views_data: sorted_views_data, mode: mode)
      end

      def calculator(mode)
        {
          total_views: ::SpLogAnalyzer::Services::TotalViewsCalculator,
          unique_views: ::SpLogAnalyzer::Services::UniqueViewsCalculator,
        }[mode]
      end
    end
  end
end
