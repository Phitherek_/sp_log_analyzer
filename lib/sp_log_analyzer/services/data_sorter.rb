# frozen_string_literal: true

require_relative 'base_service'

module SpLogAnalyzer
  module Services
    class DataSorter < BaseService
      attr_reader :views_data

      def initialize(views_data: [])
        @views_data = views_data
        super()
      end

      def call
        views_data.sort { |a, b| b[1] <=> a[1] }
      end
    end
  end
end
