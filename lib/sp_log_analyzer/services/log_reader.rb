# frozen_string_literal: true

require_relative 'base_service'
require 'csv'

module SpLogAnalyzer
  module Services
    class LogReader < BaseService
      attr_reader :errors, :log_path

      def initialize(log_path: nil)
        @errors = []
        @log_path = log_path
        super()
      end

      def call
        if log_path.nil?
          @errors << 'Log path was not provided! Please provide log path as first argument.'
          return false
        end
        CSV.read(log_path, col_sep: ' ', encoding: 'UTF-8')
      rescue StandardError => e
        @errors << "Error occurred in LogReader: #{e}"
        false
      end
    end
  end
end
