# frozen_string_literal: true

require_relative 'base_service'

module SpLogAnalyzer
  module Services
    class DataPrinter < BaseService
      attr_reader :views_data, :mode

      def initialize(views_data: [], mode: nil)
        @views_data = views_data
        @mode = mode
        super()
      end

      def call
        puts header(mode)
        puts

        views_data.each do |views_data_row|
          puts "#{views_data_row[0]} #{views_data_row[1]}#{view_type(mode) ? " #{view_type(mode)}" : ''}"
        end
      end

      def view_type(mode)
        {
          total_views: 'visits',
          unique_views: 'unique views',
        }[mode]
      end

      def header(mode)
        {
          total_views: 'Total visits:',
          unique_views: 'Unique views:',
        }[mode]
      end
    end
  end
end
