# frozen_string_literal: true

require_relative 'base_service'

module SpLogAnalyzer
  module Services
    class TotalViewsCalculator < BaseService
      attr_reader :log_data

      def initialize(log_data: [])
        @log_data = log_data
        super()
      end

      def call
        log_data.each_with_object({}) do |data_row, hash|
          hash[data_row[0]] ||= 0
          hash[data_row[0]] += 1
        end.to_a
      end
    end
  end
end
