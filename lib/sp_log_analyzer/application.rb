# frozen_string_literal: true

require_relative 'services/log_reader'
require_relative 'services/data_analyzer'

module SpLogAnalyzer
  class Application
    attr_reader :log_path

    def initialize(log_path: nil)
      @log_path = log_path
    end

    def call
      log_reader = ::SpLogAnalyzer::Services::LogReader.new(log_path: log_path)
      log_data = log_reader.call
      if log_data == false
        puts log_reader.errors.join(', ')
        return
      end

      ::SpLogAnalyzer::Services::DataAnalyzer.call(log_data: log_data, mode: :total_views)
      puts
      ::SpLogAnalyzer::Services::DataAnalyzer.call(log_data: log_data, mode: :unique_views)
    end
  end
end
