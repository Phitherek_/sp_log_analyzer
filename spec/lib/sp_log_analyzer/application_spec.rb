# frozen_string_literal: true

require_relative '../../../lib/sp_log_analyzer/application'

RSpec.describe ::SpLogAnalyzer::Application do
  describe '#call' do
    let(:log_data) do
      [%w[/test 1.1.1.1], %w[/test 1.1.1.1], %w[/test 2.2.2.2], %w[/test2 1.1.1.1], %w[/test2 2.2.2.2]]
    end

    it 'calls proper services' do
      log_reader_instance = instance_double(::SpLogAnalyzer::Services::LogReader)
      allow(::SpLogAnalyzer::Services::LogReader).to receive(:new).with(log_path: 'test_path').and_return(log_reader_instance)
      allow(log_reader_instance).to receive(:call).and_return(log_data)
      allow(::SpLogAnalyzer::Services::DataAnalyzer).to receive(:call).with(log_data: log_data, mode: :total_views).and_return(nil)
      allow(::SpLogAnalyzer::Services::DataAnalyzer).to receive(:call).with(log_data: log_data, mode: :unique_views).and_return(nil)
      expect(described_class.new(log_path: 'test_path').call).to be_nil
      expect(::SpLogAnalyzer::Services::LogReader).to have_received(:new).with(log_path: 'test_path')
      expect(log_reader_instance).to have_received(:call)
      expect(::SpLogAnalyzer::Services::DataAnalyzer).to have_received(:call).with(log_data: log_data, mode: :total_views)
      expect(::SpLogAnalyzer::Services::DataAnalyzer).to have_received(:call).with(log_data: log_data, mode: :unique_views)
    end

    it 'properly handles log reader error' do
      log_reader_instance = instance_double(::SpLogAnalyzer::Services::LogReader)
      allow(::SpLogAnalyzer::Services::LogReader).to receive(:new).with(log_path: 'test_path').and_return(log_reader_instance)
      allow(log_reader_instance).to receive(:call).and_return(false)
      allow(log_reader_instance).to receive(:errors).and_return(['Test error', 'Test error 2'])
      allow(::SpLogAnalyzer::Services::DataAnalyzer).to receive(:call).with(log_data: log_data, mode: :total_views).and_return(nil)
      allow(::SpLogAnalyzer::Services::DataAnalyzer).to receive(:call).with(log_data: log_data, mode: :unique_views).and_return(nil)
      expect { described_class.new(log_path: 'test_path').call }.to output('Test error, Test error 2
').to_stdout
      expect(::SpLogAnalyzer::Services::LogReader).to have_received(:new).with(log_path: 'test_path')
      expect(log_reader_instance).to have_received(:call)
      expect(::SpLogAnalyzer::Services::DataAnalyzer).not_to have_received(:call).with(log_data: log_data, mode: :total_views)
      expect(::SpLogAnalyzer::Services::DataAnalyzer).not_to have_received(:call).with(log_data: log_data, mode: :unique_views)
    end
  end
end
