# frozen_string_literal: true

require_relative '../../../../lib/sp_log_analyzer/services/data_sorter'

RSpec.describe ::SpLogAnalyzer::Services::DataSorter do
  describe '#call' do
    let(:views_data) do
      [['/test', 2], ['/test2', 5], ['/test3', 3]]
    end

    let(:sorted_data) do
      [['/test2', 5], ['/test3', 3], ['/test', 2]]
    end

    it 'properly sorts data' do
      expect(described_class.call(views_data: views_data)).to match_array(sorted_data)
    end

    it 'properly handles empty data' do
      expect(described_class.call).to match_array([])
    end
  end
end
