# frozen_string_literal: true

require_relative '../../../../lib/sp_log_analyzer/services/data_printer'

RSpec.describe ::SpLogAnalyzer::Services::DataPrinter do
  describe '#call' do
    let(:sorted_data) do
      [['/test2', 5], ['/test3', 3], ['/test', 2]]
    end

    let(:output1) do
      'Total visits:

/test2 5 visits
/test3 3 visits
/test 2 visits
'
    end
    let(:output2) do
      'Unique views:

/test2 5 unique views
/test3 3 unique views
/test 2 unique views
'
    end

    let(:output3) do
      '

/test2 5
/test3 3
/test 2
'
    end

    let(:output4) do
      '

'
    end

    it 'properly prints data in total views mode' do
      expect { described_class.call(views_data: sorted_data, mode: :total_views) }.to output(output1).to_stdout
    end

    it 'properly prints data in unique views mode' do
      expect { described_class.call(views_data: sorted_data, mode: :unique_views) }.to output(output2).to_stdout
    end

    it 'properly handles wrong mode' do
      expect { described_class.call(views_data: sorted_data) }.to output(output3).to_stdout
    end

    it 'properly handles empty data' do
      expect { described_class.call }.to output(output4).to_stdout
    end
  end
end
