# frozen_string_literal: true

require_relative '../../../../lib/sp_log_analyzer/services/unique_views_calculator'

RSpec.describe ::SpLogAnalyzer::Services::UniqueViewsCalculator do
  describe '#call' do
    let(:log_data) do
      [%w[/test 1.1.1.1], %w[/test 1.1.1.1], %w[/test 2.2.2.2], %w[/test2 1.1.1.1], %w[/test2 2.2.2.2]]
    end

    let(:views_data) do
      [['/test', 2], ['/test2', 2]]
    end

    it 'properly calculates unique views from given log data' do
      expect(described_class.call(log_data: log_data)).to match_array(views_data)
    end

    it 'properly handles empty log data' do
      expect(described_class.call).to match_array([])
    end
  end
end
