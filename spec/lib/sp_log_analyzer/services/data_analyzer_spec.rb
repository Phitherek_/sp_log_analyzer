# frozen_string_literal: true

require_relative '../../../../lib/sp_log_analyzer/services/data_analyzer'

RSpec.describe ::SpLogAnalyzer::Services::DataAnalyzer do
  describe '#call' do
    let(:log_data) do
      [%w[/test 1.1.1.1], %w[/test 1.1.1.1], %w[/test 2.2.2.2], %w[/test2 1.1.1.1], %w[/test2 2.2.2.2]]
    end

    let(:total_views_data) do
      [['/test', 3], ['/test2', 2]]
    end

    let(:unique_views_data) do
      [['/test', 2], ['/test2', 2]]
    end

    it 'calls proper services with total_views mode' do
      allow(::SpLogAnalyzer::Services::TotalViewsCalculator).to receive(:call).with(log_data: log_data).and_return(total_views_data)
      allow(::SpLogAnalyzer::Services::DataSorter).to receive(:call).with(views_data: total_views_data).and_return(total_views_data)
      allow(::SpLogAnalyzer::Services::DataPrinter).to receive(:call).with(views_data: total_views_data, mode: :total_views).and_return(nil)
      expect(described_class.call(log_data: log_data, mode: :total_views)).not_to be(false)
      expect(::SpLogAnalyzer::Services::TotalViewsCalculator).to have_received(:call).with(log_data: log_data)
      expect(::SpLogAnalyzer::Services::DataSorter).to have_received(:call).with(views_data: total_views_data)
      expect(::SpLogAnalyzer::Services::DataPrinter).to have_received(:call).with(views_data: total_views_data, mode: :total_views)
    end

    it 'calls proper services with unique_views mode' do
      allow(::SpLogAnalyzer::Services::UniqueViewsCalculator).to receive(:call).with(log_data: log_data).and_return(unique_views_data)
      allow(::SpLogAnalyzer::Services::DataSorter).to receive(:call).with(views_data: unique_views_data).and_return(unique_views_data)
      allow(::SpLogAnalyzer::Services::DataPrinter).to receive(:call).with(views_data: unique_views_data, mode: :unique_views).and_return(nil)
      expect(described_class.call(log_data: log_data, mode: :unique_views)).not_to be(false)
      expect(::SpLogAnalyzer::Services::UniqueViewsCalculator).to have_received(:call).with(log_data: log_data)
      expect(::SpLogAnalyzer::Services::DataSorter).to have_received(:call).with(views_data: unique_views_data)
      expect(::SpLogAnalyzer::Services::DataPrinter).to have_received(:call).with(views_data: unique_views_data, mode: :unique_views)
    end

    it 'properly handles empty call' do
      expect(described_class.call).to be(false)
    end

    it 'properly handles wrong mode' do
      expect(described_class.call(log_data: log_data, mode: :wrong)).to be(false)
    end
  end
end
