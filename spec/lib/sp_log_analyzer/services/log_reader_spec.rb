# frozen_string_literal: true

require_relative '../../../../lib/sp_log_analyzer/services/log_reader'

RSpec.describe ::SpLogAnalyzer::Services::LogReader do
  describe '#call' do
    let(:log_data) do
      [%w[/test 1.1.1.1], %w[/test 1.1.1.1], %w[/test 2.2.2.2], %w[/test2 1.1.1.1], %w[/test2 2.2.2.2]]
    end

    it 'returns false and sets proper errors when log_path is nil' do
      service = described_class.new
      expect(service.call).to be(false)
      expect(service.errors).to match_array(['Log path was not provided! Please provide log path as first argument.'])
    end

    it 'reads CSV from given path and returns proper data' do
      allow(CSV).to receive(:read).with('test_path', { col_sep: ' ', encoding: 'UTF-8' }).and_return(log_data)
      service = described_class.new(log_path: 'test_path')
      expect(service.call).to be(log_data)
      expect(CSV).to have_received(:read).with('test_path', { col_sep: ' ', encoding: 'UTF-8' })
    end

    it 'returns false and sets proper errors when exception occurs' do
      allow(CSV).to receive(:read).with('test_path', { col_sep: ' ', encoding: 'UTF-8' })
                                  .and_raise(Errno::ENOENT, 'test_path')
      service = described_class.new(log_path: 'test_path')
      expect(service.call).to be(false)
      expect(service.errors).to match_array(['Error occurred in LogReader: No such file or directory - test_path'])
      expect(CSV).to have_received(:read).with('test_path', { col_sep: ' ', encoding: 'UTF-8' })
    end
  end
end
